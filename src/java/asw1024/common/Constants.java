package asw1024.common;

/**
 * Classe contente tutte le costanti utilizzate nel portale
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class Constants {

    /********************************************************************************/
    /*********************************  COMMAND  ************************************/
    /********************************************************************************/

    /**
     * Costante che identifica il parametro della request che contiene il comando da eseguire
     */
    public static final String COMMAND_PARAM = "command";

    /**
     * Costante per il comando "fetch" dei locali
     */
    public static final String COMMAND_PLACE_FETCH = "fetch";

    /**
     * Costante per il comando "fetchById" dei locali
     */
    public static final String COMMAND_PLACE_FETCH_BY_ID = "fetchById";

    /**
     * Costante per il comando "rateUp" dei locali
     */
    public static final String COMMAND_PLACE_RATE_UP = "rateUp";

    /**
     * Costante per il comando "rateDown" dei locali
     */
    public static final String COMMAND_PLACE_RATE_DOWN = "rateDown";

    /**
     * Costante per il comando "rateRemove" dei locali
     */
    public static final String COMMAND_PLACE_RATE_REMOVE = "rateRemove";

    /**
     * Costante per il comando "fetch" dei commenti
     */
    public static final String COMMAND_COMMENT_FETCH = "fetch";

    /**
     * Costante per il comando "add" dei commenti
     */
    public static final String COMMAND_COMMENT_ADD = "add";

    /**
     * Costante per il comando "delete" dei commenti
     */
    public static final String COMMAND_COMMENT_DELETE = "delete";

    
    /********************************************************************************/
    /*********************************  ACTION  *************************************/
    /********************************************************************************/

    
    /**
     * Costante che identifica il parametro della request che contiene la action da eseguire
     */
    public static final String ACTION = "action";

    /**
     * Costante che identifica il parametro contenente la action di login 
     */
    public static final String ACTION_LOGIN = "login";

    /**
     * Costante che identifica il parametro contenente la action di logout 
     */
    public static final String ACTION_LOGOUT = "logout";

    /**
     * Costante che identifica il parametro contenente la action register 
     */
    public static final String ACTION_REGISTER = "register";

    /**
     * Costante che identifica il parametro contenente la action di update 
     */
    public static final String ACTION_UPDATE = "update";

    /********************************************************************************/
    /**********************************  PAGE  **************************************/
    /********************************************************************************/

    /* COSTANTI PER LA GESTIONE DELLE VIEW */

    /**
     * Costante che identifica il parametro della request che contiene la pagina richiesta
     */
    public static final String PAGE_REQUEST = "page";

    /**
     * Costante che identifica il parametro della request che contiene l'dentificativo della pagina di login
     */
    public static final String PAGE_REQUEST_LOGIN = "login";

    /**
     * Costante che identifica il parametro della request che contiene l'dentificativo della pagina di registrazione
     */
    public static final String PAGE_REQUEST_REGISTER = "register";

    /**
     * Costante che identifica il parametro della request che contiene l'dentificativo della pagina dei luoghi
     */
    public static final String PAGE_REQUEST_PLACE = "place";

    /**
     * Costante che identifica il parametro della request che contiene l'dentificativo della pagina "I miei luoghi"
     */
    public static final String PAGE_REQUEST_MY_PLACES = "myplace";

    /**
     * Costante che identifica il parametro della request che contiene l'dentificativo della pagina del profilo
     */
    public static final String PAGE_REQUEST_PROFILE = "profile";

    /********************************************************************************/
    /*****************************  PATH DELLE PAGINE  ******************************/
    /********************************************************************************/

    /**
     * Costante contenente il path alla pagina di errore
     */
    public static final String PAGE_ERROR = "/jsp/error.jsp";

    /**
     * Costante contenente il path alla pagina di login
     */
    public static final String PAGE_LOGIN = "/jsp/login.jsp";

    /**
     * Costante contenente il path alla pagina di registrazione
     */
    public static final String PAGE_REGISTER = "/jsp/register.jsp";

    /**
     * Costante contenente il path alla pagina del profilo
     */
    public static final String PAGE_PROFILE = "/jsp/profile.jsp";

    /**
     * Costante contenente il path alla pagina dei locali utente
     */
    public static final String PAGE_MY_PLACES = "/jsp/myplace.jsp";

    /**
     * Costante contenente il path alla home page
     */
    public static final String PAGE_HOME = "/jsp/home.jsp";

    /**
     * Costante contenente il path alla pagina dei locali
     */
    public static final String PAGE_PLACE = "/jsp/place.jsp";

    
    /********************************************************************************/
    /********************************  PARAMETRI  ***********************************/
    /********************************************************************************/
    
    /**
     * Costante per il parametro contenente il numero di pagina richiesto per i locali
     */
    public static final String PLACE_PARAM_PAGE_NUMBER = "placeListPageNumber";

    /**
     * Costante per il parametro contenente il filtro impostato per i locali
     */
    public static final String PLACE_PARAM_FILTER = "filter";

    /**
     * Costante per il parametro contenente il numero di commenti richiesti
     */
    public static final String COMMENT_PARAM_FETCH_SIZE = "commentListSize";

    /**
     * Costante per il parametro contenente l'id del locale per i commenti
     */
    public static final String COMMENT_PARAM_ID_PLACE = "idPlace";

    /**
     * Costante per il parametro contenente l'id del commento
     */
    public static final String COMMENT_PARAM_ID_COMMENT = "idComment";

    /**
     * Costante per il parametro contenente il testo di un commento
     */
    public static final String COMMENT_PARAM_TEXT = "text";

    /**
     * Costante per il parametro contenente l'id di un luogo
     */
    public static final String PLACE_PARAM_ID = "placeId";

    /********************************************************************************/
    /*********************************  SESSIONE  ***********************************/
    /********************************************************************************/

    /**
     * Costante per il parametro di sessione contenente l'utente loggato
     */
    public static final String SESSION_PARAM_LOGGED_USER = "loggedUser";

    /********************************************************************************/
    /*********************************  ATTRIBUTI  **********************************/
    /********************************************************************************/

    /**
     * Costante per l'attributo contenente la lista dei locali
     */
    public static final String ATTR_PLACE_LIST = "placeList";

    /**
     * Costante per l'attributo contenente l'utente loggato
     */
    public static final String ATTR_LOGGED_USER = "loggedUser";

    /**
     * Costante per l'attributo contenente la lista di messaggi di successo
     */
    public static final String ATTR_SUCCESS_MESSAGE = "succesMessage";

    /**
     * Costante per l'attributo contenente la lista di messaggi di errore
     */
    public static final String ATTR_ERROR_MESSAGE = "errorMessage";

    /**
     * Costante contente il numero di elemnte da visualizzare nella lista dei locali
     */
    public static final int PLACE_LIST_PAGE_SIZE = 5;

    
}
