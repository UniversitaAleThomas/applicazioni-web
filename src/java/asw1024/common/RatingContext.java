package asw1024.common;

import java.io.File;
import javax.servlet.http.HttpServletRequest;

/**
 * Classe statica contenente il contesto dell'applicazione
 *
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class RatingContext {

    private static String fileBasePath, contextPath, serverBasePath;

    /**
     * Ritorna l'url della cartella contenente le immagini
     *
     * @return l'url relativo della cartella delle immagini
     */
    public static String getImageContextPath() {
        return contextPath + "/style-sheets/images/";
    }

    /**
     * Ritorna il percorso nel filesystem alla cartella contenente le immagini
     *
     * @return Il percorso assoluto alla cartella contente le immagini
     */
    public static String getImageBasePath() {
        return fileBasePath + "/style-sheets/images/";
    }

    /**
     * Ritorna il percorso nel filesystem alla cartella contenente i file xml
     *
     * @return Il percorso assoluto alla cartella contente i file xml
     */
    public static String getXmlBasePath() {
        return fileBasePath + File.separator + "WEB-INF" + File.separator + "xml" + File.separator;
    }

    /**
     * Imposta il path assoluto nel filesystem alla cartella dell'applicazione
     *
     * @param fileBasePath Path da impostare come cartella dell'applicazione
     */
    public static void setFileBasePath(String fileBasePath) {
        RatingContext.fileBasePath = fileBasePath;
    }

    /**
     * Imposta l'url della cartella contenente i dati dell'applicazione
     *
     * @param contextPath Url da impostare come base per le chiamate
     * all'applicazione
     */
    public static void setContextPath(String contextPath) {
        RatingContext.contextPath = contextPath;
    }

    /**
     * Data una {@link HttpServletRequest request}, ricava il base URL della webapp
     * come <code>protocollo + nome : porta</code> e lo imposta nel contesto
     * @param request riferimento alla <code>HttpServletRequest</code>
     */
    public static void setServerBasePath(HttpServletRequest request) {
        if ((request.getServerPort() == 80) || (request.getServerPort() == 443)) {
            RatingContext.serverBasePath = request.getScheme() + "://" + request.getServerName();
        } else {
            RatingContext.serverBasePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        }
    }

    /**
     * Restituisce il base URL della webapp
     *
     * @return Stringa rappresentante il base URL
     */
    public static String getServerBasePath() {
        return serverBasePath;
    }

}
