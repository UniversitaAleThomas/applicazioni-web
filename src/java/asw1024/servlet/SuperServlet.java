package asw1024.servlet;

import asw1024.common.RatingContext;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static asw1024.common.Constants.*;
import asw1024.model.base.User;
import asw1024.model.response.PlaceResponseList;
import asw1024.utils.PlaceUtil;
import asw1024.utils.ResponseUtil;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamWriter;
import org.codehaus.jettison.mapped.Configuration;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;
import org.codehaus.jettison.mapped.SimpleConverter;

/**
 * Classe astratta contente una serie di metodi template e di utility per la gestione delle servlet dell'applicazione
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public abstract class SuperServlet extends HttpServlet {

    /**
     * Metodo unico che processa sia le richiest di tipo <code>GET</code> che quelle di tipo <code>POST</code>
     *
     * @param request Servlet request
     * @param response Servlet response
     * @throws ServletException In caso di errori specifici della servlet
     * @throws IOException In caso di errori di I/O
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = getServletContext().getRealPath("/");
        RatingContext.setFileBasePath(path);
        RatingContext.setContextPath(request.getContextPath());
        RatingContext.setServerBasePath(request);
        
        PlaceResponseList res = ResponseUtil.getPlacesResponseRandomImage(new User(), PlaceUtil.getPlaces());
        request.setAttribute(ATTR_PLACE_LIST, res);

        String render = request.getParameter(PAGE_REQUEST);
        String action = request.getParameter(ACTION);
        String async = request.getParameter(COMMAND_PARAM);

        String renderPage;
        if (async != null && !async.isEmpty()) {
            doAsync(request, response, async);
            response.setContentType("application/json");
            return;
        } else if (action != null && !action.isEmpty()) {
            renderPage = doAction(request, response, action);
        } else if (render != null && !render.isEmpty()) {
            renderPage = doRender(request, response, render);
        } else {
            renderPage = doRender(request, response, "default");
        }

        request.setAttribute(ATTR_LOGGED_USER, getLoggedUser(request));
        forwardPage(request, response, renderPage);
    }

    /**
     * Metodo di utility che salva l'utente in sessione
     * 
     * @param request Servlet Request
     * @param user L'utente da salvare
     */
    protected void setLoggedUser(HttpServletRequest request, User user) {
        request.getSession().setAttribute(SESSION_PARAM_LOGGED_USER, user);
    }

    /**
     * Metodo di utility che ritorna l'utente precedentemente salvato in sessione
     * 
     * @param request Servlet Request
     * @return L'utente attualmente loggato
     */
    protected User getLoggedUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(SESSION_PARAM_LOGGED_USER);
    }

    /**
     * Metodo di utility che ritorna true se è presente un utente salvato in sessione (loggato)
     * 
     * @param request Servlet Request
     * @return true se l'utente è presente, false altrimenti
     */
    protected boolean isLoggedUser(HttpServletRequest request) {
        return getLoggedUser(request) != null;
    }

    /**
     * Metodo di utility che aggiunge un messaggio di successo alla request
     * 
     * @param request Servlet Request
     * @param message Il messaggio di successo da inviare in pagina
     */
    protected void setSuccesMessage(HttpServletRequest request, String message) {
        List<String> list = (List<String>) request.getAttribute(ATTR_SUCCESS_MESSAGE);
        if (list == null) {
            list = new ArrayList<>(1);
        }
        list.add(message);
        request.setAttribute(ATTR_SUCCESS_MESSAGE, list);
    }

    /**
     * Metodo di utility che aggiunge un messaggio di errore alla request
     * 
     * @param request Servlet Request
     * @param message Il messaggio di errore da inviare in pagina
     */
    protected void setErroMessage(HttpServletRequest request, String message) {
        List<String> list = (List<String>) request.getAttribute(ATTR_ERROR_MESSAGE);
        if (list == null) {
            list = new ArrayList<>(1);
        }
        list.add(message);
        request.setAttribute(ATTR_ERROR_MESSAGE, list);
    }

    /**
     * Metodo di utility che converte l'oggetto JAXB passato come parametro in json e lo scrive nella response
     * 
     * @param response Servlet Response
     * @param clazz La classe dell'oggetto JAXB da convertire
     * @param object L'oggetto da convertire
     */
    protected void writeJSON(HttpServletResponse response, Class clazz, Object object) {
        try {
            JAXBContext jc = JAXBContext.newInstance(clazz);
            Configuration conf = new Configuration();
            conf.setTypeConverter(new SimpleConverter());
            MappedNamespaceConvention mnc = new MappedNamespaceConvention(conf);
            XMLStreamWriter xmlStreamWriter = new MappedXMLStreamWriter(mnc, new OutputStreamWriter(response.getOutputStream()));
            jc.createMarshaller().marshal(object, xmlStreamWriter);
            jc.createMarshaller().marshal(object, new MappedXMLStreamWriter(mnc, new OutputStreamWriter(System.out)));

        } catch (JAXBException | IOException ex) {
            Logger.getLogger(SuperServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo di template che viene chiamato quando una richiesta asincrona viene effettuata dalla pagina
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     * @param command Il comando richiesto dalla pagina
     */
    protected void doAsync(HttpServletRequest request, HttpServletResponse response, String command) {
    }

    /**
     * Metodo di template che viene chiamato quando viene richiesta una pagina
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     * @param page Il nome della pagina richiesta
     * @return Il path alla pagina da renderizzare
     */
    protected String doRender(HttpServletRequest request, HttpServletResponse response, String page) {
        return null;
    }

    /**
     * Metodo di template che viene chiamato quando viene richiesta l'esecuzione di una azione dalla pagina
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     * @param action L'azione da eseguire
     * @return Il path alla pagina da renderizzare dopo aver eseguito l'operazione richiesta
     */
    protected String doAction(HttpServletRequest request, HttpServletResponse response, String action) {
        return null;
    }

    ;

    /**
     * Intercetta le chiamate <code>GET</code>, le chiamate intercettate vengono ridirezionate a {@link #processRequest(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)  processRequest}.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Intercetta le chiamate <code>POST</code>, le chiamate intercettate vengono ridirezionate a {@link #processRequest(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)  processRequest}.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     *
     * @param request
     * @param response
     * @param page
     * @throws ServletException
     * @throws IOException
     */
    protected void forwardPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(page);
        rd.forward(request, response);
    }

    /**
     * Ritorna la risposta alla domanda fondamentale sulla vita, l'universo e tutto quanto
     * 
     * @return 42
     */
    @Override
    public String getServletInfo() {
        return "42";
    }

    
}
