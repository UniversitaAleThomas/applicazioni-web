package asw1024.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static asw1024.common.Constants.*;
import javax.servlet.annotation.WebServlet;

/**
 * Servlet di default
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
@WebServlet(urlPatterns = {"/"})
public class MainServlet extends SuperServlet {
    
    @Override
    protected String doRender(HttpServletRequest request, HttpServletResponse response, String page) {
        return PAGE_HOME;
    }
}
