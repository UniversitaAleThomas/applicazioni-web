package asw1024.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static asw1024.common.Constants.*;
import asw1024.model.base.Places;
import asw1024.model.base.User;
import asw1024.model.response.PlaceResponse;
import asw1024.model.response.PlaceResponseList;
import asw1024.utils.ParamUtil;
import asw1024.utils.PlaceUtil;
import asw1024.utils.RatingUtil;
import asw1024.utils.ResponseUtil;

/**
 * Servlet che si occupa di gestire tutte le richeiste relative ai locali
 *
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
@WebServlet(urlPatterns = {"/place"})
public class PlaceServlet extends SuperServlet {

    @Override
    protected String doRender(HttpServletRequest request, HttpServletResponse response, String page) {
        if (!isLoggedUser(request)) {
            return PAGE_HOME;
        }
        switch (page) {
            case PAGE_REQUEST_PLACE:
                return PAGE_PLACE;
            case PAGE_REQUEST_MY_PLACES:
                return PAGE_MY_PLACES;
        }
        return PAGE_ERROR;

    }

    @Override
    protected void doAsync(HttpServletRequest request, HttpServletResponse response, String command) {
        switch (command) {
            case COMMAND_PLACE_FETCH:
                fetchList(request, response);
                break;
            case COMMAND_PLACE_FETCH_BY_ID:
                fetchById(request, response);
                break;
            case COMMAND_PLACE_RATE_UP:
                rateUp(request, response);
                break;
            case COMMAND_PLACE_RATE_DOWN:
                rateDown(request, response);
                break;
            case COMMAND_PLACE_RATE_REMOVE:
                rateRemove(request, response);
                break;
        }
    }

    /**
     * Scrive nella response il JSON relativo alla
     * {@link PlaceResponseList PlaceResponseList} dei locali richiesti I
     * parametri relativi al numero di pagina richiesto
     * {@link #PLACE_PARAM_PAGE_NUMBER} e al filtro impostato
     * {@link #PLACE_PARAM_FILTER} vengono letti direttamente dalla request.
     *
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void fetchList(HttpServletRequest request, HttpServletResponse response) {
        int pageNumber = ParamUtil.getInteger(request, PLACE_PARAM_PAGE_NUMBER, 1);
        String filter = request.getParameter(PLACE_PARAM_FILTER);
        Places places = PlaceUtil.getPlaces();
        PlaceResponseList res = ResponseUtil.getPlacesResponse(getLoggedUser(request), places.getList(), pageNumber, PLACE_LIST_PAGE_SIZE, filter, true);
        writeJSON(response, PlaceResponseList.class, res);
    }

    /**
     * Scrive nella response il JSON del {@link PlaceResponse PlaceResponse}
     * relativo a uno specifico id, L'id del locale viene letto direttamente
     * dalla request nel parametro {@link #PLACE_PARAM_ID}
     *
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void fetchById(HttpServletRequest request, HttpServletResponse response) {
        int placeId = ParamUtil.getInteger(request, PLACE_PARAM_ID, 0);
        User u = getLoggedUser(request);

        PlaceResponse r = ResponseUtil.getPlaceResponseById(placeId, u.getId());
        writeJSON(response, PlaceResponse.class, r);
    }

    /**
     * Effettuare una votazione positiva per l'utente loggato. Il parametro
     * relativo al luogo {@link #PLACE_PARAM_ID (PLACE_PARAM_ID)} viene letto
     * direttamente dalla request Scrive nella response il JSON del nuovo
     * {@link PlaceResponse placeResponse}
     *
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void rateUp(HttpServletRequest request, HttpServletResponse response) {
        int placeId = ParamUtil.getInteger(request, PLACE_PARAM_ID, 0);
        User u = getLoggedUser(request);
        RatingUtil.rateUp(placeId, u.getId());
        PlaceResponse r = ResponseUtil.getPlaceResponseById(placeId, u.getId());
        Applet1Service.notifyRatingChanged(r);
        writeJSON(response, PlaceResponse.class, r);
    }

    /**
     * Effettuare una votazione negativa per l'utente loggato. Il parametro
     * relativo al luogo {@link #PLACE_PARAM_ID (PLACE_PARAM_ID)} viene letto
     * direttamente dalla request Scrive nella response il JSON del nuovo
     * {@link PlaceResponse placeResponse}
     *
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void rateDown(HttpServletRequest request, HttpServletResponse response) {
        int placeId = ParamUtil.getInteger(request, PLACE_PARAM_ID, 0);
        User u = getLoggedUser(request);
        RatingUtil.rateDown(placeId, u.getId());
        PlaceResponse r = ResponseUtil.getPlaceResponseById(placeId, u.getId());
        Applet1Service.notifyRatingChanged(r);
        writeJSON(response, PlaceResponse.class, r);
    }

    /**
     * Effettuare la rimozione delle votazioni per l'utente loggato. Il
     * parametro relativo al luogo {@link #PLACE_PARAM_ID (PLACE_PARAM_ID)}
     * viene letto direttamente dalla request Scrive nella response il JSON del
     * nuovo {@link PlaceResponse placeResponse}
     *
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void rateRemove(HttpServletRequest request, HttpServletResponse response) {
        int placeId = ParamUtil.getInteger(request, PLACE_PARAM_ID, 0);
        User u = getLoggedUser(request);
        RatingUtil.rateRemove(placeId, u.getId());
        PlaceResponse r = ResponseUtil.getPlaceResponseById(placeId, u.getId());
        Applet1Service.notifyRatingChanged(r);
        writeJSON(response, PlaceResponse.class, r);
    }

}
