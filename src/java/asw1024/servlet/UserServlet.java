package asw1024.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static asw1024.common.Constants.*;
import asw1024.model.base.User;
import asw1024.utils.UserUtil;

/**
 * Servlet che si occupa di servire tutte le richieste relative all'utente
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
@WebServlet(urlPatterns = {"/user"})
public class UserServlet extends SuperServlet {

    @Override
    protected String doRender(HttpServletRequest request, HttpServletResponse response, String page) {
        switch (page) {
            case PAGE_REQUEST_LOGIN:
                return PAGE_LOGIN;
            case PAGE_REQUEST_REGISTER:
                return PAGE_REGISTER;
            case PAGE_REQUEST_PROFILE:
                if (!isLoggedUser(request)) {
                    return PAGE_HOME;
                }
                return PAGE_PROFILE;
            default:
                return PAGE_ERROR;
        }
    }

    @Override
    protected String doAction(HttpServletRequest request, HttpServletResponse response, String action) {
        String page;
        switch (action) {
            case ACTION_LOGIN:
                if (login(request, response)) {
                    page = PAGE_HOME;
                } else {
                    setErroMessage(request, "Utente non valido");
                    page = PAGE_LOGIN;
                }
                break;
            case ACTION_LOGOUT:
                logout(request, response);
                page = PAGE_HOME;
                break;
            case ACTION_REGISTER:
                if (register(request, response)) {
                    page = PAGE_HOME;
                } else {
                    page = PAGE_REGISTER;
                }
                break;
            case ACTION_UPDATE:
                update(request, response);
                page = PAGE_PROFILE;
                break;

            default:
                page = PAGE_ERROR;
                break;
        }
        return page;
    }

    /**
     * Tenta di esegue il login dell'utente leggendo i parametri dalla request
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     * @return <code>true</code> se il login và a buon fine, <code>false</code> in caso contrario
     */
    protected boolean login(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("login-username");
        String password = request.getParameter("login-password");

        User user = UserUtil.findUsersByLogin(username, password);
        setLoggedUser(request, user);
        return user != null;
    }

    /**
     * Esegue il logout dell'utente svuotando la sessione
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void logout(HttpServletRequest request, HttpServletResponse response) {
        setLoggedUser(request, null);
    }
    
    /**
     * Tenta di effetuare l'aggiornamento del profilo utente leggendo i parametri dalla request
     * Se l'operazione fallisce imposta dei messaggi di errore nella request
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     * @return <code>true</code> se l'operazione và a buon fine, <code>false</code> in caso contrario
     */
    protected boolean update(HttpServletRequest request, HttpServletResponse response) {
        User loggedUser = getLoggedUser(request);

        String username = request.getParameter("user-username");
        String firstname = request.getParameter("user-firstname");
        String lastname = request.getParameter("user-lastname");
        String mail = request.getParameter("user-mail");
        String pass = request.getParameter("user-pass");
        String pass2 = request.getParameter("user-pass2");

        if (validateUser(request, username, firstname, lastname, mail, pass, pass2)) {
            User insert = UserUtil.updateUsers(loggedUser.getId(), username, firstname, lastname, mail, pass);
            if (insert == null) {
                setErroMessage(request, "Si è verificato un errore durante il salvataggio dell'utente");
                return false;
            }
            setLoggedUser(request, insert);
        }
        return true;
    }

    /**
     * Tenta di effetuare la registrazione di un nuovo utente leggendo i parametri dalla request
     * Se l'operazione fallisce imposta dei messaggi di errore nella request
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     * @return <code>true</code> se l'operazione và a buon fine, <code>false</code> in caso contrario
     */
    protected boolean register(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("user-username");
        String firstname = request.getParameter("user-firstname");
        String lastname = request.getParameter("user-lastname");
        String mail = request.getParameter("user-mail");
        String pass = request.getParameter("user-pass");
        String pass2 = request.getParameter("user-pass2");

        if (validateUser(request, username, firstname, lastname, mail, pass, pass2)) {
            User u = UserUtil.addUsers(username, firstname, lastname, mail, pass);
            if (u == null) {
                setErroMessage(request, "Si è verificato un errore durante il salvataggio dell'utente");
                return false;
            }
            setLoggedUser(request, u);
            return true;
        }
        return false;
    }

    /**
     * Effetuare la validazione dei dati utente
     * 
     * @param request Servlet Request
     * @param username Lo username dell'utente
     * @param firstname Il nome dell'utente
     * @param lastname Il cognome dell'utente
     * @param mail L'indirizzo e-mail dell'utente
     * @param pass La password dell'utente
     * @param pass2 La password di conferma 
     * @return <code>true</code> se la validazione viene completata senza errori, <code>false</code> in caso contrario
     */
    protected boolean validateUser(HttpServletRequest request, String username, String firstname, String lastname, String mail, String pass, String pass2) {
        boolean hasError = false;
        if (username == null || username.isEmpty()) {
            setErroMessage(request, "Nome utente non valido");
            hasError = true;
        }
        if (firstname == null || firstname.isEmpty()) {
            setErroMessage(request, "Nome non valido");
            hasError = true;
        }
        if (lastname == null || lastname.isEmpty()) {
            setErroMessage(request, "Cognome non valido");
            hasError = true;
        }
        if (mail == null || mail.isEmpty()) {
            setErroMessage(request, "Indirizzo e-mail non valido");
            hasError = true;
        }
        if (pass == null || pass.isEmpty()) {
            setErroMessage(request, "Password non valida");
            hasError = true;
        }
        if (!pass2.equals(pass)) {
            setErroMessage(request, "Le password inserite non corrispondono");
            hasError = true;
        }
        return !hasError;
    }

}
