package asw1024.servlet;

import asw1024.common.AsyncAdapter;
import asw1024.common.RatingContext;
import asw1024.model.base.Place;
import asw1024.model.base.Places;
import asw1024.model.base.User;
import asw1024.model.response.PlaceResponse;
import asw1024.model.response.PlaceResponseList;
import asw1024.utils.AppletConstants;
import asw1024.utils.CommentUtil;
import asw1024.utils.ManageXML;
import asw1024.utils.PlaceUtil;
import asw1024.utils.RatingUtil;
import asw1024.utils.ResponseUtil;
import asw1024.utils.UserUtil;
import asw1024.utils.AppletUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

/**
 * Servlet per la richieta di servizi da parte di Applet1
 * @author Alessandro Neri
 * @author Thomas Trapanese
 */
@WebServlet(urlPatterns = {"/Applet1"}, asyncSupported = true)
public class Applet1Service extends HttpServlet {

    private static final Logger log = Logger.getLogger(Applet1Service.class.getName());
    private static ArrayList<AsyncContext> ratingWatchers;

    /**
     * Inizializes the servlet creating useful elements, such as locks for
     * concurrency, contexts for managing notifications, paths for database
     * accesses.
     */
    @Override
    public void init() {
        ratingWatchers = new ArrayList<>();
    }

    /**
     * Funzione doPost per la gestione delle richieste di servizio.
     *
     * @param request riferimento alla {@link HttpServletRequest}
     * @param response riferimento alla {@link HttpServletResponse}
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //imposto i contesti
        RatingContext.setServerBasePath(request);

        log.log(Level.INFO, "Session ID: " + request.getSession().getId());
        
        try {
            // Estrazione e parsing dei dati XML della richiesta.
            InputStream is = request.getInputStream();
            ManageXML mngXML = new ManageXML();
            Document docrequest = mngXML.parse(is);
            is.close();

            // Si affida la gestione dell'operazione ad una funzione di utilità
            Document docresponse = mngXML.newDocument();
            if (operations(docrequest, docresponse, request)) {
                // Computata la risposta la si invia all'utente
                response.setContentType("text/xml;charset=UTF-8");
                OutputStream os = response.getOutputStream();
                mngXML.transform(os, docresponse);
                os.close();
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, "Errore durante la gestione del doPost: " + e.getMessage());
        }
    }

    /**
     * Funzione ausiliaria per discriminare tra i vari tipi di operazione richiesta
     * presente nel {@link docrequest}
     *
     * @param docrequest riferimento al documento XML con i dati della richiesta
     * @param docresponse riferimento al documento XML predisposto con i dati per la risposta
     * @param request riferimento alla {@link HttpServletRequest}
     * @return <code>true</code> se alla fine della computazione <code>docresponse</code> contiene i dati da inviare al client,
     * <code>false</code> altrimenti
     * @throws Exception
     */
    private boolean operations(Document docrequest, Document docresponse, HttpServletRequest request) throws Exception {

        boolean sendResponse = true;

        //valido la richiesta
        if (!AppletUtils.isAppletRequest(docrequest)) {
            //Non è un applet request
            AppletUtils.generateResponseDocument(docresponse, "Invalid Request XML");
            sendResponse = false;
            return sendResponse;
        }

        //Estraggo l'operazione dal document di richiesta
        String operation = AppletUtils.getRequestedOperationFromDocument(docrequest);

        switch (operation) {
            /*
             * Servizio per la richiesta dei locali dell'utente loggato
             */
            case AppletConstants.OP_GET_USR_PLACES:
                getUserPlaces(docresponse, request);
                break;
            /*
             * Servizio per il salvataggio delle modifiche ai locali utente
             */
            case AppletConstants.OP_SAVE_USR_PLACES:
                saveUserPlaces(docrequest, docresponse, request);
                break;
            /*
             * Servizio per la richiesta di rigistrazione come rating listener
             */
            case AppletConstants.OP_REGISTER_AS_RATING_LISTENER:
                registerRatingListener(request);
                sendResponse = false;
                break;
        }

        return sendResponse;
    }

    /**
     * Funzione per la gestione del servizio 'get-user-places'
     * Recupera l'utente loggato a partire dalla {@link HttpServletRequest request}, ne recupera i locali salvati su db e li
     * trasforma sul {@link Document document} docresponse
     * @param docresponse riferimento al documento XML predisposto con i dati per la risposta
     * @param request riferimento alla {@link HttpServletRequest}
     * @throws Exception
     */
    private void getUserPlaces(Document docresponse, HttpServletRequest request) throws Exception {

        //recupero l'utente loggato
        User loggedUsr = UserUtil.getLoggedUser(request);

        if (loggedUsr != null) {

            Places places = PlaceUtil.getPlacesByOwner(loggedUsr.getId());
            PlaceResponseList userPlacesResponse = ResponseUtil.getPlacesResponse(loggedUsr, places);

            //pack xml reponse
            AppletUtils.generateResponseDocument(docresponse, userPlacesResponse.getList(), AppletConstants.XML_MESSAGE_OK);

        } else {
            //Informare l'applet della mancanza del login
            AppletUtils.generateResponseDocument(docresponse, "Errore: si prega di loggarsi prima di accedere alla funzionalità di admin");
        }
    }

    /**
     * Funzione per la gestione del servizio 'save-user-places'
     * Salva i locali presenti nel {@link Document} di richiesta sotto l'id dell'utente correntemente loggato
     * Restituisce tramite un {@link Document} di risposta un messaggio di validazione della transazione ed il
     * nuovo stato dei locali dell'utente
     * @param docrequest riferimento al documento XML con i dati della richiesta
     * @param docresponse riferimento al documento XML predisposto con i dati della risposta
     * @param request riferimento alla {@link HttpServletRequest}
     * @throws Exception
     */
    private synchronized void saveUserPlaces(Document docrequest, Document docresponse, HttpServletRequest request) throws Exception {

        //decodifico i locali dal Document di risposta
        List<PlaceResponse> elencoLocaliModificati = AppletUtils.getUserPlacesFromDocument(docrequest);

        //recupero l'utente loggato
        User loggedUsr = UserUtil.getLoggedUser(request);

        if (elencoLocaliModificati != null) {
            //Arrivato un aggiornamento dei locali utente
            Places dbLocali = PlaceUtil.getPlaces();
            Places dbLocaliUtente = PlaceUtil.getPlacesByOwner(loggedUsr.getId());

            int maxId = dbLocali.getMaxId();

            //Controllo le aggiunte/modifiche ai locali
            for (PlaceResponse localeModificato : elencoLocaliModificati) {

                if (AppletConstants.ID_NEW_PLACE == localeModificato.getId()) {
                    //to add and continue
                    Place p = new Place();
                    p.setId(++maxId);
                    p.setDescription(localeModificato.getDescription());
                    p.setName(localeModificato.getName());
                    p.setAddress(localeModificato.getAddress());
                    p.setIdUsrOwner(loggedUsr.getId());

                    PlaceUtil.setPlaceImage(p.getId(), localeModificato.getImagePath());

                    dbLocali.getList().add(p);
                } else {
                    //Recupero il locale originale dal db per applicare le modifiche dell'utente
                    for (Place localeOriginale : dbLocali.getList()) {
                        if (localeOriginale.getId() == localeModificato.getId()) {
                            //riporto le modifiche
                            localeOriginale.setDescription(localeModificato.getDescription());
                            localeOriginale.setName(localeModificato.getName());
                            localeOriginale.setAddress(localeModificato.getAddress());

                            PlaceUtil.setPlaceImage(localeModificato.getId(), localeModificato.getImagePath());
                        }
                    }
                }
            }

            dbLocali.setMaxId(maxId);

            //Controllo se l'utente ha rimosso dei locali personali
            for (Place localeUtenteOriginale : dbLocaliUtente.getList()) {

                //Se nel pannello di amministrazione l'utente ha rimosso un locale,
                //nella lista dei localiModificati non comparirà il suo pkid
                boolean toRemove = true;
                for (PlaceResponse localeUtenteModificato : elencoLocaliModificati) {
                    if (localeUtenteOriginale.getId() == localeUtenteModificato.getId()) {
                        //Il locale esiste ancora
                        toRemove = false;
                        break;
                    }
                }

                if (toRemove) {
                    //rimuoviamo ogni riferimento al locale
                    CommentUtil.removeCommentByIdPlace(localeUtenteOriginale.getId());
                    RatingUtil.removeRatingsByIdPlace(localeUtenteOriginale.getId());
                    PlaceUtil.removePlaceImage(localeUtenteOriginale.getId());
                    PlaceUtil.removePlaceById(dbLocali, localeUtenteOriginale.getId());
                }
            }

            //Salviamo l'elaborazione
            PlaceUtil.savePlaces(dbLocali);

        }

        //informo l'applet del salvataggio corretto dei dati
        //e della nuova situazione dell'utente loggato
        Places savedPlacesList = PlaceUtil.getPlacesByOwner(loggedUsr.getId());
        PlaceResponseList savedPlaceResponseList = ResponseUtil.getPlacesResponse(loggedUsr, savedPlacesList);
        AppletUtils.generateResponseDocument(docresponse, savedPlaceResponseList.getList(), AppletConstants.XML_MESSAGE_OK);
    }

    /**
     * Funzione per la gestione del servizio 'register-rating-listener'
     * Salva un riferimento al contesto asincrono di una {@link HttpServletRequest},
     * registrandola come listener dei cambiamenti dei voti dei locali
     * @param request riferimento alla {@link HttpServletRequest}
     */
    private void registerRatingListener(HttpServletRequest request) {

        // Per alcune versioni di Tomcat è necessario esplicitamente richiedere il supporto per le servlet asincrone
        if (!request.isAsyncSupported()) {
            request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        }

        AsyncContext asyncContext = request.startAsync();
        asyncContext.setTimeout(20 * 1000);     //20 Secondi
        asyncContext.addListener(new AsyncAdapter() {
            @Override
            public void onTimeout(AsyncEvent e) {
                try {
                    ManageXML mngXML = new ManageXML();
                    Document answer = mngXML.newDocument();
                    AppletUtils.generateResponseDocument(answer, AppletConstants.XML_MESSAGE_TIMEOUT);

                    AsyncContext asyncContext = e.getAsyncContext();

                    //Invio il timeout solo se il listener è ancora in listening
                    boolean confirm;
                    synchronized (Applet1Service.this) {
                        if ((confirm = ratingWatchers.contains(asyncContext))) {
                            ratingWatchers.remove(asyncContext);
                        }
                    }
                    if (confirm) {
                        OutputStream tos = asyncContext.getResponse().getOutputStream();
                        mngXML.transform(tos, answer);
                        tos.close();
                        asyncContext.complete();
                    }
                } catch (Exception ex) {
                    log.log(Level.SEVERE, "Errore durante la comunicazione del timeout " + e.getAsyncContext().toString() + ": " + ex.getMessage());
                }
            }
        });
        synchronized (this) {
            ratingWatchers.add(asyncContext);
        }
    }

    /**
     * Funzione per la notifica delle informazioni riguardo al voto di un locale a tutti i listener in ascolto
     * @param newRate {@link PlaceResponse} contenente le informazioni sui voti del locale
     */
    public static synchronized void notifyRatingChanged(PlaceResponse newRate) {

        ArrayList<AsyncContext> ratingListener = Applet1Service.getRatingWatchers();

        if (ratingListener == null) {
            //nessun listener da notificare
            return;
        }
        try {
            //Genero il pacchetto da inviare a tutti i listener
            ManageXML xmlManager = new ManageXML();
            Document docresponse = xmlManager.newDocument();
            AppletUtils.generateResponseDocument(docresponse, newRate, AppletConstants.XML_MESSAGE_OK);
//            AppletUtils.prettyPrintDocument(docresponse, System.out);
            
            //Comunico la situazione del locale a tutti i listeners
            for (AsyncContext asyncContext : ratingListener) {
                //per ogni watcher dei rating trasmetto il nuovo dato
                try {
                    OutputStream aos = asyncContext.getResponse().getOutputStream();
                    xmlManager.transform(aos, docresponse);
                    aos.close();
                    asyncContext.complete();
                } catch (IOException | TransformerException e) {
                    log.log(Level.SEVERE, "Errore durante la notifica di variazione voti al listener: " + e.getMessage());
                }
            }
            
        } catch (TransformerConfigurationException | ParserConfigurationException ex) {
            log.log(Level.SEVERE, "Errore durante la generazione dei dati per i listener: " + ex.getMessage());
        }
        //Tutti i listener notificati! Pulisco la coda
        ratingListener.clear();
    }

    /**
     * Getter per i listener dei rating dei locali
     * @return Lista di {@link AsyncContext} dei listener
     */
    public static ArrayList<AsyncContext> getRatingWatchers() {
        return ratingWatchers;
    }

}
