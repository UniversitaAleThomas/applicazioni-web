package asw1024.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static asw1024.common.Constants.*;
import asw1024.model.base.Comment;
import asw1024.model.base.User;
import asw1024.model.response.CommentResponse;
import asw1024.model.response.CommentResponseList;
import asw1024.model.response.GenericResponse;
import asw1024.utils.CommentUtil;
import asw1024.utils.ParamUtil;
import asw1024.utils.ResponseUtil;

/**
 * Servlet che si occupa di servire tutte le richieste relative ai commenti
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends SuperServlet {

    @Override
    protected void doAsync(HttpServletRequest request, HttpServletResponse response, String command) {
        switch (command) {
            case COMMAND_COMMENT_FETCH:
                fetchList(request, response);
                break;
            case COMMAND_COMMENT_ADD:
                add(request, response);
                break;
            case COMMAND_COMMENT_DELETE:
                delete(request, response);
                break;
        }
    }

    /**
     * Scrive nella response la lista JSON dei commenti richiesti.
     * I parametri relativi alla dimensione dei commenti ({@link #COMMENT_PARAM_FETCH_SIZE}) 
     * e all'id del locale per cui si richiedono i commenti {@link #COMMENT_PARAM_ID_PLACE} 
     * vengono letti direttamente dalla request
     * Nella response viene scritto il JSON relativo alla {@link CommentResponseList CommentResponseList} richiesta.
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void fetchList(HttpServletRequest request, HttpServletResponse response) {
        Integer size = ParamUtil.getInteger(request, COMMENT_PARAM_FETCH_SIZE, 1);
        Integer idPlace = ParamUtil.getInteger(request, COMMENT_PARAM_ID_PLACE, 0);

        CommentResponseList resp = ResponseUtil.getCommentResponse(idPlace, size, getLoggedUser(request));
        writeJSON(response, CommentResponseList.class, resp);
    }

    /**
     * Aggiunge un nuovo commento.
     * I parametri relativi al testo del commento da inserire ({@link #COMMENT_PARAM_TEXT}) 
     * e all'id del locale cui si riferisce il commento {@link #COMMENT_PARAM_ID_PLACE} 
     * vengono letti direttamente dalla request
     * Nella response viene scritto il JSON relativo al {@link CommentResponse CommentResponse} appena inserito.
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void add(HttpServletRequest request, HttpServletResponse response) {
        String text = request.getParameter(COMMENT_PARAM_TEXT);
        Integer idPlace = ParamUtil.getInteger(request, COMMENT_PARAM_ID_PLACE, 1);
        User u = getLoggedUser(request);
        Comment c = CommentUtil.addComment(idPlace, u.getId(), text);
        CommentResponse resp = ResponseUtil.getCommentResponse(c, u);
        writeJSON(response, CommentResponse.class, resp);
    }

    /**
     * Cancella un commento.
     * L'id del commento da rimuovere viene letto direttamente dalla request {@link #COMMENT_PARAM_ID_COMMENT} 
     * Nella response viene scritto il JSON relativo a un {@link GenericResponse GenericResponse} 
     * che contiene un messaggio di errore se l'operazione è fallita o di successo se è andata a buon fine.
     * 
     * @param request Servlet Request
     * @param response Servlet Response
     */
    protected void delete(HttpServletRequest request, HttpServletResponse response) {
        Integer idComment = ParamUtil.getInteger(request, COMMENT_PARAM_ID_COMMENT, 0);
        if (CommentUtil.removeComment(idComment)) {
            writeJSON(response, GenericResponse.class, new GenericResponse(true, "Commento Eliminato"));
        } else {
            writeJSON(response, GenericResponse.class, new GenericResponse(false, "Errore durante l'eliminazione del commento"));
        }
    }
}
