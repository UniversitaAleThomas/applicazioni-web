package asw1024.utils;

import asw1024.model.base.Comment;
import asw1024.model.base.Comments;
import asw1024.model.base.Place;
import asw1024.model.base.Places;
import asw1024.model.base.Ratings;
import asw1024.model.base.User;
import asw1024.model.base.Users;
import asw1024.model.response.CommentResponse;
import asw1024.model.response.CommentResponseList;
import asw1024.model.response.PlaceResponse;
import asw1024.model.response.PlaceResponseList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Classe statica di util contente tutti i metodi relativi alla gestione delle
 * risposte JAXB da mandare in pagina
 *
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class ResponseUtil {

    /**
     * Ritorna una lista di CommentResponse
     *
     * @param idPlace Id del locale
     * @param size Dimensione della lista
     * @param loggedUser Utente loggato
     * @return L'oggetto {@link CommentResponseList CommentResponseList}
     * contenente la lista dei commenti richiesti
     */
    public static CommentResponseList getCommentResponse(int idPlace, int size, User loggedUser) {
        Comments comments = CommentUtil.getCommentByPlaceId(idPlace, true);
        Users u = UserUtil.findUsers();
        List<CommentResponse> listComm = new ArrayList<>();
        int total = comments.getList().size();
        for (Comment c : comments.getList()) {
            if (listComm.size() >= size) {
                break;
            }
            listComm.add(getCommentResponse(c, u, loggedUser));
        }

        return new CommentResponseList(listComm, total);
    }

    /**
     * Ritorna una lista casuale di locali che hanno una immagine valida
     *
     * @param loggedUser Utente loggato
     * @param places Elenco di locali su cui eseguire l'operazione
     * @return L'oggetto {@link PlaceResponseList PlaceResponseList} contenente
     * la lista dei locali richiesti
     */
    public static PlaceResponseList getPlacesResponseRandomImage(User loggedUser, Places places) {
        List<Place> placeList = places.getList();
        List<Place> randomPlaceList = new ArrayList<>(placeList.size());
        Random rnd = new Random();
        for (Place p : placeList) {

            int index = 0;
            if (randomPlaceList.size() > 0) {
                index = rnd.nextInt(randomPlaceList.size());
            }
            if (PlaceUtil.placeHasImage(p.getId())) {
                randomPlaceList.add(index, p);
            }
        }

        return getPlacesResponse(loggedUser, randomPlaceList, 1, -1, "", false);
    }

    /**
     * Ritorna la lista dei locali salvati
     *
     * @param loggedUser Utente loggato
     * @param places Elenco di locali su cui eseguire l'operazione
     * @return L'oggetto {@link PlaceResponseList PlaceResponseList} contenente
     * la lista dei locali richiesti
     */
    public static PlaceResponseList getPlacesResponse(User loggedUser, Places places) {
        return getPlacesResponse(loggedUser, places.getList(), 1, -1, "", false);
    }

    /**
     * Ritorna la lista paginata dei locali salvati
     *
     * @param loggedUser Utente loggato
     * @param places Elenco di locali su cui eseguire l'operazione
     * @param pageNumber Numero di pagina richiesto
     * @param pageSize Dimensione della pagina
     * @param sort Se true ordina il risultato in base al ratign del locale (dal
     * più alto al più basso)
     * @return L'oggetto {@link PlaceResponseList PlaceResponseList} contenente
     * la lista dei locali richiesti
     */
    public static PlaceResponseList getPlacesResponse(User loggedUser, List<Place> places, int pageNumber, int pageSize, String filter, boolean sort) {
        List<PlaceResponse> ret = new ArrayList<>();
        for (Place p : places) {
            Ratings ratings = RatingUtil.getRating();
            int positiveRatings = RatingUtil.countPositive(ratings, p.getId());
            int negativeRatings = RatingUtil.countNegative(ratings, p.getId());
            int userRatings = RatingUtil.getUserRating(ratings, loggedUser.getId(), p.getId());

            PlaceResponse tmp = new PlaceResponse();
            tmp.setId(p.getId());
            tmp.setName(p.getName());
            tmp.setAddress(p.getAddress());
            tmp.setDescription(p.getDescription());
            tmp.setImagePath(PlaceUtil.getWebPlaceImagePath(p.getId()));
            tmp.setPositiveRatings(positiveRatings);
            tmp.setNegativeRatings(negativeRatings);
            tmp.setUserRating(userRatings);

            //owner info
            User owner = UserUtil.findUserById(p.getIdUsrOwner());
            if (owner != null) {
                tmp.setOwnerName(owner.getFullName());
            }

            ret.add(tmp);
        }

        if (filter != null && !filter.trim().isEmpty()) {
            ret = filterPlacesResponse(ret, filter);
        }
        if (sort) {
            ret = sortPlacesResponse(ret);
        }
        int total = ret.size();
        int min = pageSize * (pageNumber - 1);
        int max = pageSize * pageNumber;
        if (max > total || pageSize == -1) {
            max = total;
        }

        return new PlaceResponseList(ret.subList(min, max), total, pageNumber, pageSize);
    }

    /**
     * Funzione che applica un filtro ad una lista di {@link PlaceResponse}
     * @param places La lista di {@link PlaceResponse} da filtrare
     * @param filter Il filtro da applicare
     * @return La lista di {@link PlaceResponse} filtrata
     */
    protected static List<PlaceResponse> filterPlacesResponse(List<PlaceResponse> places, String filter) {
        String[] filterArr = filter.split(" ");
        List<PlaceResponse> tmp = new ArrayList<>();
        for (PlaceResponse pr : places) {
            boolean add = true;
            for (String f : filterArr) {
                add &= pr.getName().toLowerCase().contains(f.trim().toLowerCase());
            }
            if (add) {
                tmp.add(pr);
            }
        }
        return tmp;
    }

    /**
     * Funzione che applica un ordinamento ad una lista di {@link PlaceResponse}
     * @param places La lista di {@link PlaceResponse} da ordinare
     * @return La lista di {@link PlaceResponse} ordinata
     */
    protected static List<PlaceResponse> sortPlacesResponse(List<PlaceResponse> places) {
        Collections.sort(places, new Comparator<PlaceResponse>() {
            @Override
            public int compare(PlaceResponse t1, PlaceResponse t2) {
                int val1 = (t1.getPositiveRatings() - t1.getNegativeRatings());
                int val2 = (t2.getPositiveRatings() - t2.getNegativeRatings());
                return Integer.compare(val2, val1);
            }
        });
        return places;
    }

    /**
     * Restituisce il locale con uno specifico id cercandolo all'interno di una
     * lista di locali
     *
     * @param userId Id dell'utente loggato
     * @param placeId Id del locale richiesto
     * @return L'oggetto {@link PlaceResponse PlaceResponse} relativo al locale
     * richiesto
     */
    public static PlaceResponse getPlaceResponseById(int placeId, int userId) {
        Places places = PlaceUtil.getPlaces();
        Place p = PlaceUtil.getPlaceById(places, placeId);

        Ratings ratings = RatingUtil.getRating();
        int positiveRatings = RatingUtil.countPositive(ratings, p.getId());
        int negativeRatings = RatingUtil.countNegative(ratings, p.getId());
        int userRatings = RatingUtil.getUserRating(ratings, userId, p.getId());

        PlaceResponse tmp = new PlaceResponse();
        tmp.setId(p.getId());
        tmp.setName(p.getName());
        tmp.setDescription(p.getDescription());
        tmp.setImagePath(PlaceUtil.getWebPlaceImagePath(p.getId()));
        tmp.setPositiveRatings(positiveRatings);
        tmp.setNegativeRatings(negativeRatings);
        tmp.setUserRating(userRatings);

        return tmp;
    }

    /**
     * Converte un {@link Comment Comment} in un
     * {@link CommentResponse CommentResponse}
     *
     * @param c Il commento da convertire
     * @param loggedUser Utente loggato
     * @return L'oggetto {@link CommentResponse CommentResponse} relativo al
     * commento richiesto
     */
    public static CommentResponse getCommentResponse(Comment c, User loggedUser) {
        Users u = UserUtil.findUsers();
        return getCommentResponse(c, u, loggedUser);
    }

    /**
     * Converte un {@link Comment Comment} in un
     * {@link CommentResponse CommentResponse}
     *
     * @param c Il commento da convertire
     * @param u Lista di utenti su cui effettuare le operazioni di ricerca delle
     * informazioni dell'utente proprietario del commento
     * @param loggedUser Utente loggato
     * @return L'oggetto {@link CommentResponse CommentResponse} relativo al
     * commento richiesto
     */
    public static CommentResponse getCommentResponse(Comment c, Users u, User loggedUser) {
        User tmpUser = UserUtil.findUserById(u, c.getUserId());
        CommentResponse tmp = new CommentResponse();
        tmp.setId(c.getId());
        tmp.setText(c.getText());
        tmp.setUserId(c.getUserId());
        tmp.setUserFullName(tmpUser.getFullName());
        tmp.setDate(c.getDate());
        tmp.setEditable(c.getUserId() == loggedUser.getId());

        return tmp;
    }

}
