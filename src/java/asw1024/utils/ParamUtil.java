package asw1024.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * Classe statica di util
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class ParamUtil {
    
    /**
     * Cerca un parametro nella request e tenta di castarlo a un Integer
     * 
     * @param request Servlet Request
     * @param param Nome del parametro
     * @param defaultValue Valore di default
     * @return Il valore ottenuto dalla request se di tipo intero, default value in caso contrario
     */
    public static final Integer getInteger(HttpServletRequest request, String param, Integer defaultValue) {
        String par = request.getParameter(param);
        Integer parValue;
        try {
            parValue = Integer.parseInt(par);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
        return parValue;
    }
}
