package asw1024.utils;

import asw1024.common.RatingContext;
import asw1024.model.base.Comment;
import asw1024.model.base.Comments;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * Classe statica di util contente tutti i metodi relativi alla gestione della
 * persistenza dei commenti
 *
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class CommentUtil {

    /**
     * Costante contente il nome del file xml su cui salvare i commenti
     */
    protected static final String COMMENT_XML_FILENAME = "Comments.xml";

    /**
     * Getter per i commenti salvati
     *
     * @return ritorna un oggetto {@link Comments} con la lista dei commenti
     */
    public static Comments getComments() {
        try {
            JAXBContext jc = JAXBContext.newInstance(Comments.class);
            return (Comments) jc.createUnmarshaller().unmarshal(new File(RatingContext.getXmlBasePath() + COMMENT_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
        }
        return null;
    }

    /**
     * Getter per i commenti salvati
     *
     * @param sort Se true ordina il risultato dal più recente al più vecchio
     * @return ritorna un oggetto {@link Comments} con la lista dei commenti
     */
    public static Comments getComments(boolean sort) {
        Comments com = getComments();
        if (sort) {
            Collections.sort(com.getList(), new Comparator<Comment>() {

                @Override
                public int compare(Comment t1, Comment t2) {
                    if (t1.getDateTime() != null) {
                        return t1.getDateTime().compareTo(t2.getDateTime());
                    }
                    return 0;
                }
            });
        }
        return com;
    }

    /**
     * Ritorna tutti i commento relativi a un locale
     *
     * @param idPlace Id del locale di cui si richiedono i commenti
     * @param sort Se true ordina il risultato dal più recente al più vecchio
     * @return Ritorna un oggetto {@link Comments} con la lista dei commenti
     */
    public static Comments getCommentByPlaceId(int idPlace, boolean sort) {
        Comments allComments = getComments(sort);
        List<Comment> tmpList = new ArrayList<>();
        for (Comment c : allComments.getList()) {
            if (c.getPlaceId() == idPlace) {
                tmpList.add(0, c);
            }
        }
        return new Comments(tmpList);
    }

    /**
     * Ritorna tutti i commento relativi a un locale ordinati per data dal più
     * recente al più vecchio
     *
     * @param placeId Id del locale a cui si riferisce il commento
     * @param userId Id dell'utente che ha lasciato il commento
     * @param text Il testo del commento da salvare
     * @return Ritorna un oggetto {@link Comment} relativo al commento appena
     * inserito
     */
    public static Comment addComment(Integer placeId, Integer userId, String text) {
        Comments comments = getComments();

        Comment c = new Comment(comments.getMaxId() + 1, placeId, userId, text, new Date());
        comments.getList().add(c);
        comments.setMaxId(c.getId());
        if (saveComments(comments)) {
            return c;
        }
        return null;

    }

    /**
     * Data una lista di {@link Comments Commenti} li salva su file xml
     *
     * @param comments Oggetto contenente i commenti da salvare
     * @return Ritorna <code>true</code> se l'operazione và a buon fine,
     * <code>false</code> in caso contrario
     */
    protected static boolean saveComments(Comments comments) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Comments.class);
            jc.createMarshaller().marshal(comments, new File(RatingContext.getXmlBasePath() + COMMENT_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
            return false;
        }
        return true;
    }

    /**
     * Elimina un commento
     *
     * @param idComment Id del commento da rimuovere
     * @return Ritorna <code>true</code> se l'operazione và a buon fine,
     * <code>false</code> in caso contrario
     */
    public static boolean removeComment(int idComment) {
        Comments comments = getComments();
        boolean ret = false;
        for (Comment c : comments.getList()) {
            if (c.getId() == idComment) {
                comments.getList().remove(c);
                ret = true;
                break;
            }
        }
        if (ret) {
            ret = saveComments(comments);
        }
        return ret;
    }

    /**
     * Rimuove dal database tutti i commenti di un dato locale
     *
     * @param idPlace Id del locale di cui rimuovere i commenti
     */
    public static void removeCommentByIdPlace(int idPlace) {
        Comments comments = getComments();
        for (Iterator<Comment> iterator = comments.getList().iterator(); iterator.hasNext();) {
            Comment c = iterator.next();
            if (c.getPlaceId() == idPlace) {
                iterator.remove();
            }
        }
        saveComments(comments);
    }

}
