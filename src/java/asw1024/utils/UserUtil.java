package asw1024.utils;

import asw1024.common.Constants;
import asw1024.common.RatingContext;
import asw1024.model.base.User;
import asw1024.model.base.Users;
import java.io.File;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * Classe statica di util contente tutti i metodi relativi alla gestione degli
 * utenti
 *
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class UserUtil {

    /**
     * Costante contente il nome del file xml su cui salvare gli utenti
     */
    protected static final String USER_XML_FILENAME = "Users.xml";

    /**
     * Restituisce l'utente correntemente loggato
     *
     * @param request HttpServletRequest che rappresenta la richiesta
     * @return L'istanza di {@link User User} correntemente loggato
     */
    public static User getLoggedUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(Constants.SESSION_PARAM_LOGGED_USER);
    }

    /**
     * Restituisce la lista degli utenti salvati
     *
     * @return L'istanza di {@link Users Users} contenente la lista degli utenti
     */
    public static Users findUsers() {
        try {
            JAXBContext jc = JAXBContext.newInstance(Users.class);
            return (Users) jc.createUnmarshaller().unmarshal(new File(RatingContext.getXmlBasePath() + USER_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
        }
        return null;
    }

    /**
     * Cerca se è presente un utente con lo username e la password passate come
     * parametro
     *
     * @param username Lo username da ricercare
     * @param password La password dell'utente da verificare
     * @return L'istanza di {@link User User} se l'utente viene trovato, null in
     * caso contrario
     */
    public static User findUsersByLogin(String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return null;
        }
        Users users = findUsers();
        if (users == null) {
            return null;
        }
        User user = null;
        for (User u : users.getList()) {
            if (u.getUserName().equals(username) && u.getPassword().equals(password)) {
                user = u;
                break;
            }
        }
        return user;
    }

    /**
     * Cerca se è presente un utente con un certo id
     *
     * @param id Id dell'utente da ricercare
     * @return L'istanza di {@link User User} se l'utente viene trovato, null in
     * caso contrario
     */
    public static User findUserById(int id) {
        Users users = findUsers();
        for (User u : users.getList()) {
            if (u.getId() == id) {
                return u;
            }
        }
        return null;
    }

    /**
     * Cerca se è presente un utente con un certo id
     *
     * @param users Lista di utenti su cui effettuare la ricerca
     * @param id dell'utente da ricercare
     * @return L'istanza di {@link User User} se l'utente viene trovato, null in
     * caso contrario
     */
    public static User findUserById(Users users, int id) {
        for (User u : users.getList()) {
            if (u.getId() == id) {
                return u;
            }
        }
        return null;
    }

    /**
     * Aggiorna il profili di un utente
     *
     * @param id Id dell'utente da aggiornare
     * @param username Lo username dell'utente
     * @param firstname Il nome dell'utente
     * @param lastname Il cognome dell'utente
     * @param mail L'indirizzo e-mail dell'utente
     * @param pass La password dell'utente
     * @return L'utente modificato
     */
    public static User updateUsers(int id, String username, String firstname, String lastname, String mail, String pass) {
        Users users = findUsers();

        for (User u : users.getList()) {
            if (u.getId() == id) {
                u.setFirstName(firstname);
                u.setLastName(lastname);
                u.setUserName(username);
                u.setEmail(mail);
                u.setPassword(pass);
                saveUsers(users);
                return u;
            }
        }
        return null;
    }

    /**
     * Aggiunge un nuovo utente
     *
     * @param username Lo username dell'utente
     * @param firstname Il nome dell'utente
     * @param lastname Il cognome dell'utente
     * @param mail L'indirizzo e-mail dell'utente
     * @param pass La password dell'utente
     * @return L'istanza di {@link User User} contenente l'utente appena
     * inserito se l'operazione và a buon fine, null in caso contrario
     */
    public static User addUsers(String username, String firstname, String lastname, String mail, String pass) {
        Users users = findUsers();

        User u = new User(users.getMaxId() + 1, username, firstname, lastname, mail, pass);
        users.getList().add(u);
        users.setMaxId(u.getId());
        if (saveUsers(users)) {
            return u;
        }
        return null;
    }


    /**
     * Data una lista di {@link Users Utenti} li salva su file xml
     * 
     * @param users Oggetto contenente gli utenti da salvare
     * @return Ritorna <code>true</code> se l'operazione và a buon fine, <code>false</code> in caso contrario
     */
    protected static boolean saveUsers(Users users) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Users.class);
            jc.createMarshaller().marshal(users, new File(RatingContext.getXmlBasePath() + USER_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
            return false;
        }
        return true;
    }

}
