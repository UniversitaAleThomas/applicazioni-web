package asw1024.utils;

import asw1024.common.RatingContext;
import asw1024.model.base.Place;
import asw1024.model.base.Places;
import asw1024.servlet.SuperServlet;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;


/**
 * Classe statica di util contente tutti i metodi relativi alla gestione della persistenza dei locali
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class PlaceUtil {

    /**
     * Costante contente il nome del file xml su cui salvare i locali
     */
    protected static final String PLACE_XML_FILENAME = "Places.xml";

    /**
     * Getter per i locali salvati
     * 
     * @return ritorna un oggetto {@link Places} con la lista dei locali 
     */
    public static Places getPlaces() {
        try {
            JAXBContext jc = JAXBContext.newInstance(Places.class);
            return (Places) jc.createUnmarshaller().unmarshal(new File(RatingContext.getXmlBasePath() + PLACE_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
        }
        return null;
    }

    /**
     * Restituisce l'elenco dei locali in base all'id del proprietario selezionato
     *
     * @param idUsrOwner Id del proprietario dei locali
     * @return ritorna un oggetto {@link Places} con la lista dei locali 
     */
    public static Places getPlacesByOwner(int idUsrOwner) {
        List<Place> ret = new ArrayList<>();

        for (Place p : getPlaces().getList()) {
            if (p.getIdUsrOwner() == idUsrOwner) {
                ret.add(p);
            }
        }

        return new Places(ret);
    }

    /**
     * Restituisce il locale con uno specifico id cercandolo all'interno di una lista di locali
     * 
     * @param places Oggetto contenente una lista di locale
     * @param idPlace Id del locale da ricercare
     * @return L'oggetto {@link Place Place} rappresentativo del locale, null se non presente 
     */
    public static Place getPlaceById(Places places, int idPlace) {
        for (Place p : places.getList()) {
            if (p.getId() == idPlace) {
                return p;
            }
        }
        return null;
    }

    /**
     * Dato l'id di un locale, lo rimuove dalla lista passata
     *
     * @param places Oggetto contenente una lista di locale
     * @param idPlace Id del locale da ricercare
     */
    public static void removePlaceById(Places places, int idPlace) {
        for (Iterator<Place> iterator = places.getList().iterator(); iterator.hasNext();) {
            Place p = iterator.next();
            if (p.getId() == idPlace) {
                iterator.remove();
            }
        }
    }

    /**
     * Persiste i locali su xml
     *
     * @param places I <code>Places</code> locali da salvare
     */
    public static void savePlaces(Places places) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Places.class);
            jc.createMarshaller().marshal(places, new File(RatingContext.getXmlBasePath() + PLACE_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Controlla se il locale ha una immagine di profilo
     *
     * @param idPlace Identificatore del <code>Place</code> di cui controllare la presenza dell'immagine
     * @return true se esiste, false altrimenti
     */
    public static boolean placeHasImage(int idPlace) {
        String realPath = RatingContext.getImageBasePath() + idPlace + ".jpg";
        return new File(realPath).exists();
    }

    /**
     * Restituisce il percorso all'immagine di profilo del locale selezionato
     *
     * @param idPlace Identificatore del <code>Place</code> di cui recuperare la foto profilo
     * @return L'URL all'immagine di profilo se esiste, l'immagine di default altrimenti
     */
    public static String getPlaceImagePath(int idPlace) {
        String path = RatingContext.getImageContextPath() + idPlace + ".jpg";

        if (placeHasImage(idPlace)) {
            return path;
        } else {
            return RatingContext.getImageContextPath() + "foto_non_disponibile.jpg";
        }
    }

    /**
     * Restituisce il percorso web all'immagine di profilo del locale selezionato
     *
     * @param idPlace Identificatore del <code>Place</code> di cui recuperare la foto profilo
     * @return L'URL all'immagine di profilo se esiste, l'immagine di default altrimenti
     */
    public static String getWebPlaceImagePath(int idPlace) {
        String path = getPlaceImagePath(idPlace);

        if (path.startsWith("/")) {
            //è una immagine relativa al portale -> la rendo web
            path = RatingContext.getServerBasePath() + path;
        }
        
        return path;
    }

    /**
     * Salva la foto del locale selezionato a partire dall'URL di origine
     *
     * @param idPlace <code>Place</code> di cui recuperare la foto profilo
     * @param imgURL L'URL dell'Immagine da salvare come foto del locale
     */
    public static void setPlaceImage(int idPlace, String imgURL) {
        if (idPlace < 0 || imgURL == null || imgURL.isEmpty() || imgURL.equals(RatingContext.getServerBasePath() + RatingContext.getImageContextPath() + "foto_non_disponibile.jpg")) {
            return;
        }
        try {

            if (imgURL.startsWith("/")) {
                //è una immagine relativa al portale -> la rendo web
                imgURL = RatingContext.getServerBasePath() + imgURL;
            }

            URL url = new URL(imgURL);
            BufferedImage image = ImageIO.read(url);

            String physicalPath = RatingContext.getImageBasePath() + idPlace + ".jpg";
            File dest = new File(physicalPath);

            if (placeHasImage(idPlace)) {
                //Il locale aveva già una foto profilo
                dest.delete();
            }

            ImageIO.write(image, "jpg", dest);

        } catch (Exception e) {
            Logger.getLogger(SuperServlet.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * Rimuove la foto del locale selezionato
     *
     * @param idPlace <code>Place</code> di cui rimuovere la foto profilo
     */
    public static void removePlaceImage(int idPlace) {
        if (idPlace < 0) {
            return;
        }
        try {
            String path = RatingContext.getImageContextPath() + idPlace + ".jpg";
            File dest = new File(path);
            if (dest.exists()) {
                //Il locale aveva già una foto profilo
                dest.delete();
            }
        } catch (Exception e) {
            Logger.getLogger(SuperServlet.class.getName()).log(Level.SEVERE, null, e);
        }

    }

}
