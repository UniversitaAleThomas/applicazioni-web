package asw1024.utils;

import asw1024.common.RatingContext;
import asw1024.model.base.Rating;
import asw1024.model.base.Ratings;
import java.io.File;
import java.util.Iterator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;


/**
 * Classe statica di util contente tutti i metodi relativi alla gestione dei rating dei locali
 * 
 * @author Thomas Trapanese
 * @author Alessandro Neri
 */
public class RatingUtil {

    /**
     * Costante contente il nome del file xml su cui salvare i rating dei locali
     */
    protected static final String RATING_XML_FILENAME = "Rating.xml";

    /**
     * Getter per i rating salvati
     * 
     * @return ritorna un oggetto {@link Ratings} con la lista dei rating 
     */
    public static Ratings getRating() {
        try {
            JAXBContext jc = JAXBContext.newInstance(Ratings.class);
            return (Ratings) jc.createUnmarshaller().unmarshal(new File(RatingContext.getXmlBasePath() + RATING_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
        }
        return null;
    }

    /**
     * Si occupa di persistere i rating passati come parametro su file xml
     * 
     * @param ratings Lista dei rating da persistere
     */
    public static void saveRating(Ratings ratings) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Ratings.class);
            jc.createMarshaller().marshal(ratings, new File(RatingContext.getXmlBasePath() + RATING_XML_FILENAME));
        } catch (JAXBException ex) {
            System.err.println(ex);
        }
    }
    
    /**
     * Conta il numero di votazioni positive per un certo locale 
     * 
     * @param ratings Lista di rating in cui cercare
     * @param placeId Id del locale
     * @return Il numero di votazioni positive
     */
    public static int countPositive(Ratings ratings, int placeId) {
        int ret = 0;
        for (Rating col : ratings.getList()) {
            if(col.getPlaceId() == placeId && col.getValue() > 0) ret ++;
        }
        return ret;
    }
    
    /**
     * Conta il numero di votazioni negative per un certo locale 
     * 
     * @param ratings Lista di rating in cui cercare
     * @param placeId Id del locale
     * @return Il numero di votazioni negative
     */
    public static int countNegative(Ratings ratings, int placeId) {
        int ret = 0;
        for (Rating col : ratings.getList()) {
            if(col.getPlaceId() == placeId && col.getValue() < 0) ret ++;
        }
        return ret;
    }

    /**
     * Ritorna il voto lasciato da un utente ad un determinato locale cercandolo in una lista passata come parametro
     * 
     * @param ratings Lista di rating in cui cercare
     * @param userId Id dell'utente
     * @param placeId Id del locale
     * @return Il rating laciato dall'utente al locale se presente, null in caso contrario
     */
    public static int getUserRating(Ratings ratings, int userId, int placeId) {
        for (Rating col : ratings.getList()) {
            if(col.getUserId() == userId && col.getPlaceId() == placeId) return col.getValue();
        }
        return 0;
    }

    /**
     * Aggiunge o aggiorna una votazione
     * 
     * @param placeId Id del locale oggetto della votazione
     * @param userId Id dell'uUtente che lascia il voto
     * @param value valore della votazione
     */
    protected static void setRating(int placeId, int userId, int value) {
        Ratings ratings = getRating();
        boolean found = false;
        for (Rating r : ratings.getList()) {
            if(r.getUserId() == userId && r.getPlaceId() == placeId) {
                r.setValue(value);
                found = true;
                break;
            }
        }

        if(!found) {
            Rating r = new Rating(ratings.getMaxId() + 1, placeId, userId, value);
            ratings.getList().add(r);
            ratings.setMaxId(r.getId());
        }
        
        saveRating(ratings);
    }
    

    /**
     * Aggiunge una votazione positiva o ne aggiorna una precedente
     * 
     * @param placeId Id del locale oggetto della votazione
     * @param userId Id dell'uUtente che lascia il voto
     */
    public static void rateUp(int placeId, int userId) {
        setRating(placeId, userId, 1);
    }

    /**
     * Aggiunge una votazione negativa o ne aggiorna una precedente
     * 
     * @param placeId Id del locale oggetto della votazione
     * @param userId Id dell'uUtente che lascia il voto
     */
    public static void rateDown(int placeId, int userId) {
        setRating(placeId, userId, -1);
    }

    /**
     * Se presente rimuove una votazione
     * 
     * @param placeId Id del locale oggetto della votazione
     * @param userId Id dell'uUtente che lascia il voto
     */
    public static void rateRemove(int placeId, int userId) {
        Ratings ratings = getRating();
        boolean found = false;
        for (Rating r : ratings.getList()) {
            if(r.getUserId() == userId && r.getPlaceId() == placeId) {
                found = true;
                ratings.getList().remove(r);
                break;
            }
        }

        if(found) {
            saveRating(ratings);
        }
    }
    
    /**
     * Rimuove dal database tutti i ratings di un dato locale
     * @param idPlace Id del locale di cui rimuovere i ratings
     */
    public static void removeRatingsByIdPlace(int idPlace) {
        Ratings ratings = getRating();
        for (Iterator<Rating> iterator = ratings.getList().iterator(); iterator.hasNext();) {
            Rating r = iterator.next();
            if (r.getPlaceId() == idPlace) {
                iterator.remove();
            }
        }
        saveRating(ratings);
    }
}
