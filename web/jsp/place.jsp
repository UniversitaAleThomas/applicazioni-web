<%@page import="asw1024.common.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Public Rating</title>
        <%@ include file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <header>            
            <%@ include file="/WEB-INF/jspf/top.jspf" %>
        </header>
        <div id="content">
            <%@ include file="/WEB-INF/jspf/content-top-place.jspf" %>
            <div id="sidebar" data-bind="css: {'hidden' : !placeDetail()  }">
                <%@ include file="/WEB-INF/jspf/content-slideshow.jspf" %>
                <%@ include file="/WEB-INF/jspf/sidebar.jspf" %> 
            </div>
            <div data-bind="css: {'hidden' : placeDetail()  }">
                <%@ include file="/WEB-INF/jspf/content-list.jspf" %>
            </div>
            <div class="view-content" data-bind="css: {'hidden' : !placeDetail()  }">
                <%@ include file="/WEB-INF/jspf/content-detail.jspf" %>
            </div>
            <%
                String placeId = request.getParameter("placeId");
                if (placeId == null) {
                    placeId = "";
                }
            %>
            <script type="text/javascript">
                document.addEventListener("DOMContentLoaded", function () {
                    // Activates knockout.js
                    ko.applyBindings(new MasterViewModel(<%= placeId%>));
                });
            </script>
        </div>
        <div class="separator"></div>
        <footer>
            <%@ include file="/WEB-INF/jspf/bottom.jspf" %>
        </footer>
    </body>
</html>
