<%@page import="asw1024.common.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Public Rating</title>
        <%@ include file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <header>            
            <%@ include file="/WEB-INF/jspf/top.jspf" %>
        </header>
        <div id="content">
            <div id="sidebar">
                <%@ include file="/WEB-INF/jspf/content-slideshow.jspf" %> 
                <%@ include file="/WEB-INF/jspf/sidebar.jspf" %> 
            </div>
            <%@ include file="/WEB-INF/jspf/content-top.jspf" %>
            <div class="view-content">
                <h2>Si è verificato un errore</h2>
            </div>
        </div>
        <div class="separator"></div>
        <footer>
            <%@ include file="/WEB-INF/jspf/bottom.jspf" %>
        </footer>
    </body>
</html>
