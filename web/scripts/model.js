/**
 * Modello dei commenti
 * @method CommentModel
 * @param {number} id Id del commento
 * @param {number} userId Id dell'autore del commento
 * @param {number} placeId Id del luogo cui si riferisce il commento
 * @param {string} author Nome completo dell'autore del commento
 * @param {string} date Data in cui è stato lasciato il commento
 * @param {string} text Testo del commento
 * @param {boolean} editable Indica se il commento è rimuovibile dall'utente 
 * (possibile solo se l'utente che ha lasciato il commento corrisponde all'utente loggato)
 */
function CommentModel(id, userId, placeId, author, date, text, editable) {
    this.id         = ko.observable(id);
    this.userId     = ko.observable(userId);
    this.placeId    = ko.observable(placeId);
    this.cAuthor    = ko.observable(author);
    this.cDate      = ko.observable(date);
    this.cText      = ko.observable(text);
    this.editable   = ko.observable(editable);
}    
/**
 * Modello per gestire liste in lazy-loading di commenti
 * @method CommentListModel
 * @param {number} count Indica il numero totale di commenti presenti
 * @param {CommentModel} comments Contiene la lista di commenti caricati
 */
function CommentListModel( count, comments ) {
    self = this;
    self.count = ko.observable(count);
    self.list = ko.observableArray(comments);
    /*
     * Metodo computed che ritorna il numero di commenti ancora da visualizare
     * return {number} Il numero di commenti ancora da visualizzare
     */
    self.remaining = ko.computed(function() {
        return self.count() - self.list().length;
    }, self);
}

/**
 * Modello dei locali
 * @method PlaceModel
 * @param {number} id Id del locale
 * @param {string} name Nome del locale
 * @param {string} description Descrizione del locale
 * @param {string} address Indirizzo  del locale
 * @param {number} userRating Voto lasciato dall'utente corrente al locale
 * @param {number} totalRatingPositiveCount Numero complessivo di voti positivi del locale
 * @param {number} totalRatingNegativeCount Numero complessivo di voti negativi del locale
 * @param {string} imagePath Percorso all'immagine del locale
 */
function PlaceModel(id, name, description, address, userRating, totalRatingPositiveCount, totalRatingNegativeCount, imagePath) {
    this.id                             = ko.observable(id);
    this.name                           = ko.observable(name);
    this.description                    = ko.observable(description);
    this.address                        = ko.observable(address);
    this.userRating                     = ko.observable(userRating);
    this.totalRatingPositiveCount       = ko.observable(totalRatingPositiveCount);
    this.totalRatingNegativeCount       = ko.observable(totalRatingNegativeCount);
    this.imagePath                      = ko.observable(imagePath);
    /*
     * Metodo computed che ritorna il voto effettivo del locale
     * return {number} Il rating del locale
     */
    this.rating = ko.computed(function() {
        return this.totalRatingPositiveCount() - this.totalRatingNegativeCount();
    }, this);
    
}

/**
 * Modello per gestire liste paginate di locali
 * @method PlaceListModel
 * @param {number} total Numero totale di locali memorizzati
 * @param {number} pageNumber Numero della pagina a cui si riferisce la lista contenuta in places
 * @param {number} pageSize Numero di record per pagina 
 * @param {number} places Lista dei locali a pagina pageNumber
 */
function PlaceListModel( total, pageNumber, pageSize, places ) {
    self = this;
    self.total = ko.observable(total);
    self.pageNumber = ko.observable(pageNumber);
    self.pageSize = ko.observable(pageSize);
    self.list = ko.observableArray(places);
    /*
     * Metodo computed che ritorna il numero totale di pagine
     * return {number} Il numero di pagine totali
     */
    self.pageCount = ko.computed(function() {
        return Math.ceil(self.total() / self.pageSize());
    }, self);
}