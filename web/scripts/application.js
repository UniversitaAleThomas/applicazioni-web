/**
 * File javascript contenente il ViewModel 
 * utilizzato nella gestione delle visualizzazioni 
 * dei locali in modalità lista e dettaglio
 * @method MasterViewModel
 * @param {number} selectedPlaceId L'eventuale luogo di cui visualizzare il dettaglio
 */
function MasterViewModel(selectedPlaceId) {
    /**
     * Alias per mantenere il riferimento al ViewModel nelle funzioni interne
     * @type MasterViewModel
     */
    that = this;

    /**
     * Observable array contenente i messaggi da visualizzare in pagina. 
     * 
     * I messaggi hanno la struttura:
     *  {
     *      state: (true|false), 
     *      messagge: (messaggio)
     *  }
     *  
     * Dove state indica il tipo di messaggio, true indica un messaggio di successo,
     * false un messaggio di errore.
     * Messagge contiene il testo del messaggio da visualizzare
     * @type ObservableArray(messages)
     * */
    that.message = ko.observableArray();

    /**
     * Variabile booleana che indica la modalità di visualizzazione per i locali, 
     *      false visualizza la lista
     *      true  visualizza il dettaglio
     * @type boolean
     */
    that.placeDetail = ko.observable(false);

    /**
     * Observable contente la lista dei locali da visualizzare, 
     * @type {@link PlaceListModel}
     */
    that.placeList = ko.observable();
    
    /**
     * Observable contente il numero di pagina attualmente viualizzata, 
     * @type {@link PlaceListModel}
     */
    that.placePageNumber = ko.observable();
    
    /**
     * Contiene l'ultimo luogo selezionato
     * @type {@link PlaceModel}
     */
    that.selectedPlace = ko.observable(new PlaceModel('', '', '', 0, 0));

    /**
     * Observable contente la lista dei commenti da visualizzare per il locale memorizzato in {@link that.selectedPlace}, 
     * @type {@link CommentListModel}
     */
    that.comment = ko.observable();
    
    /**
     * Observable contente il filtro impostato per la lista dei locali 
     * @type String
     */
    that.filter = ko.observable();

    /*********************************************************************************************************/
    /*****************************  METODI PER LA GESTIONE DEI MESSAGGI  *************************************/
    /*********************************************************************************************************/

    /**
     * Metodo che aggiunge un messaggio a {@link that.message}, previene l'inserimento di messaggi duplicati.
     * @method addMessage
     * @param {message} msg Il messaggio da aggiungere
     */
    that.addMessage = function (msg) {
        that.deleteMessage(msg);    //Cancello i messaggi uguali già presenti
        that.message.push(msg);     //Inserisco il messaggio nuovo
    };

    /**
     * Metodo che elimina un messaggio da {@link that.message}.
     * @method deleteMessage
     * @param {message} msg Il messaggio da eliminare
     */
    that.deleteMessage = function (msg) {
        var item;
        ko.utils.arrayForEach(that.message(), function (v) {
            if (v.state === msg.state && v.message === msg.message) {
                item = v;
            }
        });
        var arr = that.message();
        ko.utils.arrayRemoveItem(arr, item);
        that.message(arr);
    };


    /*********************************************************************************************************/
    /******************************  METODI PER LA GESTIONE DEI LOCALI  **************************************/
    /*********************************************************************************************************/

    /**
     * Metodo che invoca i servizi per ottenere la lista dei locati relativa alla pagina num
     * @method refreshPlace
     */
    that.refreshPlace = function () {
        Service.Place.fetch(that.placePageNumber(), that.filter(), function (ret) {
            that.placeList(ret);
        }, that.addMessage);
    };

    /**
     * Metodo che imposta il numero di pagina selezionato e invoca il refresh dei locali
     * @method loadPlace
     * @param {number} num il numero di pagina selezionato
     */
    that.loadPlace = function (num) {
        that.placePageNumber(num);
        that.refreshPlace();
    };


    /**
     * Metodo che cambia la visualizzazione dei locali da lista a dettaglio
     * @method showDetail
     * @param {PlaceModel} place Il locale selezionato
     */
    that.showDetail = function (place) {
        that.selectedPlace(place);
        that.placeDetail(true);
        that.loadComment();
    };

    /**
     * Metodo che cambia la visualizzazione dei locali da dettaglio a lista
     * @method showList
     * @param {PlaceModel} place Il locale selezionato
     */
    that.showList = function () {
        that.refreshPlace();
        that.placeDetail(false);
    };

    /**
     * Metodo che richiede ai servizi di aggiungere una votazione positiva sul luogo selezionato in {@link that.selectedPlace} 
     * se non erano presenti votazioni positive da parte dell'utente, in caso contrario chiede la rimozaione della votazione
     * @method rateUp
     */
    that.rateUp = function () {
        if (!that.selectedPlace().id() || that.selectedPlace().id() === "")
            return;
        /**
         * Metodo di callback, se la modifica del rating ha successo si occupa di aggiornare il viewModel
         * @method succ
         * @param {PlaceModel} ret contiene il nuovo PlaceModel modificato
         */
        var succ = function (ret) {
            that.selectedPlace(ret);
        };
        if (that.selectedPlace().userRating() > 0) {
            Service.Place.rateRemove(that.selectedPlace().id(), succ, that.addMessage);
        } else {
            Service.Place.rateUp(that.selectedPlace().id(), succ, that.addMessage);
        }
    };

    /**
     * Metodo che richiede ai servizi di aggiungere una votazione negativa sul luogo selezionato in {@link that.selectedPlace}
     * se non erano presenti votazioni negative da parte dell'utente, in caso contrario chiede la rimozaione della votazione
     * @method rateUp
     */
    that.rateDown = function () {
        if (!that.selectedPlace().id() || that.selectedPlace().id() === "")
            return;
        /**
         * Metodo di callback, se la modifica del rating ha successo si occupa di aggiornare il viewModel
         * @method succ
         * @param {PlaceModel} ret contiene il nuovo PlaceModel modificato
         */
        var succ = function (ret) {
            that.selectedPlace(ret);
        };
        if (that.selectedPlace().userRating() < 0) {
            Service.Place.rateRemove(that.selectedPlace().id(), succ, that.addMessage);
        } else {
            Service.Place.rateDown(that.selectedPlace().id(), succ, that.addMessage);
        }
    };

    /*********************************************************************************************************/
    /*****************************  METODI PER LA GESTIONE DEI COMMENTI  *************************************/
    /*********************************************************************************************************/

    /**
     * Ricarica i commenti presenti in {@link that.comment}
     * @method refreshComment
     */
    that.refreshComment = function () {
        var size = that.comment().list().length;
        Service.Comment.fetch(that.selectedPlace().id(), size, function (ret) {
            that.comment(ret);
        }, that.addMessage);
    };

    /**
     * Effettua il caricamento dei commenti, se già presenti ne richiede altri 3 al server
     * @method loadComment
     */
    that.loadComment = function () {
        var size = 3;
        if (that.comment() !== undefined && that.comment().list() !== null) {
            size += that.comment().list().length;
        }
        Service.Comment.fetch(that.selectedPlace().id(), size, function (data) {
            that.comment(data);
        }, that.addMessage)
    };

    /**
     * Richiede ai servizi la rimozione del commento passato come parametro
     * @method removeComment
     * @param {CommentModel} comm Il commento da rimuovere
     */
    that.removeComment = function (comm) {
        Service.Comment.delete(comm.id(), function (ret) {
            that.addMessage(ret);   //Aggiungo il messaggio di ritorno
            $('.comment-wrapper[data-id=' + comm.id() + ']').slideUp(500, function () {
                that.refreshComment();
            }); //Eseguo una piccola animazione al termine della quale richiedo il refresh della lista dei commenti
        }, that.addMessage);
    };


    /**
     * Richiede ai servizi l'aggiunta di un nuovo commento
     * @method newComment
     */
    that.newComment = function () {
        //recupero il testo inserito dall'utente
        var txt = document.getElementById('newCommentText').value;
        //effettuo la chiamata ai servizi
        Service.Comment.add(that.selectedPlace().id(), txt, function (ret) {
            //se và tutto bene aggiungo il commento in cima alla lista
            that.comment().list.unshift(ret);
            //svuoto la textArea del messaggio
            document.getElementById('newCommentText').value = "";
            //ed eseguo una piccola animazione per nascondere la textArea
            $('#newCommentContainer').slideUp();
        }, that.addMessage);
    };

    /**
     * Se il form per l'aggiunta di un nuovo commento di un nuovo commento non è visibile lo mosta, viceversa lo nasconde.
     * @method showNewComment
     */
    that.showNewComment = function () {
        if (document.getElementById('newCommentContainer').style.display === "none") {
            $('#newCommentContainer').slideDown();
            document.getElementsByClassName('detail-wrapper')[0].getElementsByClassName('fa-comment-o')[0].className += " rating-rated";
        } else {
            $('#newCommentContainer').slideUp();
            document.getElementsByClassName('detail-wrapper')[0].getElementsByClassName('fa-comment-o')[0].className = document.getElementsByClassName('detail-wrapper')[0].getElementsByClassName('fa-comment-o')[0].className.replace(" rating-rated", "");
        }
    };

    /**
     * Funzione di inizializzazione del ViewModel.
     * Effettua il caricamento iniziale della lista dei luoghi
     * @method init
     */
    that.init = function () {
        Service.Place.fetch(1, "", function (ret) {
            that.placeList(ret);
        }, that.addMessage);
        if (selectedPlaceId) {
            Service.Place.fetchById(selectedPlaceId, function (ret) {
                that.showDetail(ret);
            }, that.addMessage);
        }
    };
    
    that.init(); //Inizializzazione del ViewModel
}

