document.addEventListener('DOMContentLoaded', function () {
    var mess = document.getElementsByClassName('message');
    for (i = 0; i < mess.length; i++) {
        var msg = mess[i];
        console.log(mess[i].children[0]);
        mess[i].children[0].onclick = function () {
            this.parentNode.remove();
        };
    }
}, false);

$(document).ready(function () {
    var slideShow = function (itemArr, init) {
        var next = (init + 1) % itemArr.length;
        $(itemArr[init]).delay(2000).fadeOut(500, function () {
            $(itemArr[next]).fadeIn(500, function () {
                slideShow(itemArr, next);
            });
        });
    };
    slideShow($('.slide-wrapper .slide-content'), 0);
});