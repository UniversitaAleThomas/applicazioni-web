/**
 * Contenitore per le funzioni statiche di accesso alla logica lato server
 * @method Service
 */
function Service() {
}
;
Service.COMMAND_PARAM = "command";

/**
 * Contentitore per tutte le chiamate AJAX che riguardano la gestione dei commenti
 * @method Comment
 */
Service.Comment = function () {
};
/** 
 * Costante contenente l'url relativo della servlet che si occupa di gestire le chiamate AJAX relative ai commenti
 * @constant 
 * @type {string}
 */
Service.Comment.SERVLET_URL = "comment";
/** 
 * Costante per il comando "add" dei commenti
 * @constant 
 * @type {string}
 */
Service.Comment.COMMAND_ADD = "add";
/** 
 * Costante per il comando "fetch" dei commenti
 * @constant 
 * @type {string}
 */
Service.Comment.COMMAND_FETCH = "fetch";
/** 
 * Costante per il comando "delete" dei commenti
 * @constant 
 * @type {string}
 */
Service.Comment.COMMAND_DELETE = "delete";

/**
 * Contentitore per tutte le chiamate AJAX che riguardano la gestione dei luoghi
 * @method Place
 */
Service.Place = function () {
};
/** 
 * Costante contenente l'url relativo della servlet che si occupa di gestire le chiamate AJAX relative ai luoghi
 * @constant 
 * @type {string}
 */
Service.Place.SERVLET_URL = "place";
/** 
 * Costante per il comando "fetch" dei commenti
 * @constant 
 * @type {string}
 */
Service.Place.COMMAND_FETCH = "fetch";
/** 
 * Costante per il comando "fetchById" dei commenti
 * @constant 
 * @type {string}
 */
Service.Place.COMMAND_FETCH_BY_ID = "fetchById";
/** 
 * Costante per il comando "rateUp" dei commenti
 * @constant 
 * @type {string}
 */
Service.Place.COMMAND_RATE_UP = "rateUp";
/** 
 * Costante per il comando "rateDown" dei commenti
 * @constant 
 * @type {string}
 */
Service.Place.COMMAND_RATE_DOWN = "rateDown";
/** 
 * Costante per il comando "rateRemove" dei commenti
 * @constant 
 * @type {string}
 */
Service.Place.COMMAND_RATE_REMOVE = "rateRemove";

/**
 * Effettua una chiamata AJAX alla servlet dei commenti che si occupa del salvataggio del nuovo commento
 * @method add
 * @param {number} placeId L'id del locale a cui si riferisce il commento
 * @param {string} text Il testo del commento
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link CommentModel}
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Comment.add = function (placeId, text, onSuccess, onError) {
    $.ajax({
        url: Service.Comment.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Comment.COMMAND_ADD, idPlace: placeId, text: text},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var m_id, m_userId, m_placeId, m_author, m_date, m_text, m_editable;
            m_id = remResp.commentResponse.id;
            m_userId = remResp.commentResponse.userId;
            m_placeId = remResp.commentResponse.placeId;
            m_author = remResp.commentResponse.userFullName;
            m_date = remResp.commentResponse.date;
            m_text = remResp.commentResponse.text;
            m_editable = remResp.commentResponse.editable === 'true';
            onSuccess(new CommentModel(m_id, m_userId, m_placeId, m_author, m_date, m_text, m_editable));
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};
/**
 * Interroga la servlet per ottenere una lista di commenti relativi a un posto
 * @method fetch
 * @param {number} placeId L'id del locale a cui si riferiscono i commenti
 * @param {number} size Numero di commenti da richiedere
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link CommentListModel}
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Comment.fetch = function (placeId, size, onSuccess, onError) {
    $.ajax({
        url: Service.Comment.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Comment.COMMAND_FETCH, commentListSize: size, idPlace: placeId},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var listaRet = [];
            var count = remResp.commentResponseList.total;
            if (remResp.commentResponseList.list instanceof Array) {

                for (var a = 0; a < remResp.commentResponseList.list.length; a++) {
                    listaRet.push(
                            new CommentModel(
                                    remResp.commentResponseList.list[a].id,
                                    remResp.commentResponseList.list[a].userId,
                                    remResp.commentResponseList.list[a].placeId,
                                    remResp.commentResponseList.list[a].userFullName,
                                    remResp.commentResponseList.list[a].date,
                                    remResp.commentResponseList.list[a].text,
                                    remResp.commentResponseList.list[a].editable === 'true')
                            );
                }
            } else if(remResp.commentResponseList.list) {
                listaRet.push(
                        new CommentModel(
                                remResp.commentResponseList.list.id,
                                remResp.commentResponseList.list.userId,
                                remResp.commentResponseList.list.placeId,
                                remResp.commentResponseList.list.userFullName,
                                remResp.commentResponseList.list.date,
                                remResp.commentResponseList.list.text,
                                remResp.commentResponseList.list.editable === 'true')
                        );

            }

            onSuccess(new CommentListModel(count, listaRet));
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */

        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};
/**
 * Effettua una chiamata AJAX alla servlet dei commenti che si occupa dell'eliminazione del commento con id commentId
 * @method delete
 * @param {number} commentId Id del commento da rimuovere
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un messaggio
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Comment.delete = function (commentId, onSuccess, onError) {
    $.ajax({
        url: Service.Comment.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Comment.COMMAND_DELETE, idComment: commentId},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            onSuccess({state: remResp.genericResponse.state === 'true', message: remResp.genericResponse.message});
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};

/**
 * Interroga la servlet per ottenere una lista paginata dei luoghi
 * @method fetch
 * @param {number} pageNumber numero della pagina richiesta
 * @param {string} filter il filtro usato per la selezione
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link PlaceListModel}
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Place.fetch = function (pageNumber, filter, onSuccess, onError) {
    $.ajax({
        url: Service.Place.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Place.COMMAND_FETCH, placeListPageNumber: pageNumber, filter: filter},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var listaRet = [];
            var total = remResp.placeResponseList.total;
            var pageSize = remResp.placeResponseList.pageSize;
            pageNumber = remResp.placeResponseList.pageNumber;
            if (remResp.placeResponseList.list instanceof Array) {
                for (var a = 0; a < remResp.placeResponseList.list.length; a++) {
                    listaRet.push(
                            new PlaceModel(
                                    remResp.placeResponseList.list[a].id,
                                    remResp.placeResponseList.list[a].name,
                                    remResp.placeResponseList.list[a].description,
                                    remResp.placeResponseList.list[a].address,
                                    remResp.placeResponseList.list[a].userRating,
                                    remResp.placeResponseList.list[a].positiveRatings,
                                    remResp.placeResponseList.list[a].negativeRatings,
                                    remResp.placeResponseList.list[a].imagePath)
                            );
                }
            } else if(remResp.placeResponseList.list){
                listaRet.push(
                        new PlaceModel(
                                remResp.placeResponseList.list.id,
                                remResp.placeResponseList.list.name,
                                remResp.placeResponseList.list.description,
                                remResp.placeResponseList.list.address,
                                remResp.placeResponseList.list.userRating,
                                remResp.placeResponseList.list.positiveRatings,
                                remResp.placeResponseList.list.negativeRatings,
                                remResp.placeResponseList.list.imagePath)
                        );

            }
            onSuccess(new PlaceListModel(total, pageNumber, pageSize, listaRet));
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};

/**
 * Interroga la servlet per ottenere il luogo con id placeId
 * @method fetchById
 * @param {number} placeId Id del luogo richiesto
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link PlaceModel}
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Place.fetchById = function (placeId, onSuccess, onError) {
    $.ajax({
        url: Service.Place.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Place.COMMAND_FETCH_BY_ID, placeId: placeId},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var ret = new PlaceModel(
                    remResp.placeResponse.id,
                    remResp.placeResponse.name,
                    remResp.placeResponse.description,
                    remResp.placeResponse.address,
                    remResp.placeResponse.userRating,
                    remResp.placeResponse.positiveRatings,
                    remResp.placeResponse.negativeRatings,
                    remResp.placeResponse.imagePath);
            onSuccess(ret);
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};

/**
 * Richiede alla servlet di salvare un voto positivo da parte dell'utente loggato relativamente al luogo di id placeId
 * @method rateUp
 * @param {number} placeId Id del luogo al quale di riferisce la votazione
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link PlaceModel} 
 * contenente il modello del luogo aggiornato dopo la votazione
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Place.rateUp = function (placeId, onSuccess, onError) {
    $.ajax({
        url: Service.Place.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Place.COMMAND_RATE_UP, placeId: placeId},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var ret = new PlaceModel(
                    remResp.placeResponse.id,
                    remResp.placeResponse.name,
                    remResp.placeResponse.description,
                    remResp.placeResponse.address,
                    remResp.placeResponse.userRating,
                    remResp.placeResponse.positiveRatings,
                    remResp.placeResponse.negativeRatings,
                    remResp.placeResponse.imagePath);
            onSuccess(ret);
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};

/**
 * Richiede alla servlet di rimuovere ogni voto dell'utente loggato relativo al luogo di id placeId
 * @method rateRemove
 * @param {number} placeId Id del luogo al quale di riferisce la votazione
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link PlaceModel} 
 * contenente il modello del luogo aggiornato dopo la votazione
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Place.rateRemove = function (placeId, onSuccess, onError) {
    $.ajax({
        url: Service.Place.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Place.COMMAND_RATE_REMOVE, placeId: placeId},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var ret = new PlaceModel(
                    remResp.placeResponse.id,
                    remResp.placeResponse.name,
                    remResp.placeResponse.description,
                    remResp.placeResponse.address,
                    remResp.placeResponse.userRating,
                    remResp.placeResponse.positiveRatings,
                    remResp.placeResponse.negativeRatings,
                    remResp.placeResponse.imagePath);
            onSuccess(ret);
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};
/**
 * Richiede alla servlet di salvare un voto negativo da parte dell'utente loggato relativamente al luogo di id placeId
 * @method rateDown
 * @param {number} placeId Id del luogo al quale di riferisce la votazione
 * @param {function} onSuccess Funzione di callback, in caso di successo verrà passata come parametro un'istanza di {@link PlaceModel} 
 * contenente il modello del luogo aggiornato dopo la votazione
 * @param {function} onError Funzione di callback, in caso di fallimento verrà passato un messaggio di errore
 */
Service.Place.rateDown = function (placeId, onSuccess, onError) {
    $.ajax({
        url: Service.Place.SERVLET_URL,
        type: "POST",
        dataType: 'json',
        data: {command: Service.Place.COMMAND_RATE_DOWN, placeId: placeId},
        /**
         * Funzione chiamata in caso di succcesso dell'operazione, 
         * @method success
         * @param {json} remResp Risposta json del server
         */
        success: function (remResp) {
            var ret = new PlaceModel(
                    remResp.placeResponse.id,
                    remResp.placeResponse.name,
                    remResp.placeResponse.description,
                    remResp.placeResponse.address,
                    remResp.placeResponse.userRating,
                    remResp.placeResponse.positiveRatings,
                    remResp.placeResponse.negativeRatings,
                    remResp.placeResponse.imagePath);
            onSuccess(ret);
        },
        /**
         * Funzione chiamata in caso di errori di comunicazione col server
         * @method error
         */
        error: function () {
            onError({state: false, message: 'Errore di connessione'});
        }
    });
};